/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.teste.clientelimitecreditorisco.service;

import br.com.teste.cliente.model.Cliente;
import br.com.teste.cliente.service.ClienteService;
import br.com.teste.clientelimitecreditorisco.dao.ClienteLimiteCreditoRiscoDAO;
import br.com.teste.clientelimitecreditorisco.mapping.ClienteLimiteCreditoRiscoDTO;
import br.com.teste.clientelimitecreditorisco.mapping.ClienteLimiteCreditoRiscoDTOMapper;
import br.com.teste.clientelimitecreditorisco.model.ClienteLimiteCreditoRisco;
import br.com.teste.common.NegocioException;
import br.com.teste.limitecredito.model.LimiteCredito;
import br.com.teste.limitecredito.service.LimiteCreditoService;
import br.com.teste.risco.model.Risco;
import br.com.teste.risco.service.RiscoService;

import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Elivelton de Macedo
 */
@Service
public class ClienteLimiteCreditoRiscoServiceImpl implements ClienteLimiteCreditoRiscoService {

    private final ClienteLimiteCreditoRiscoDAO clienteLimiteCreditoRiscoDAO;

    private final ClienteLimiteCreditoRiscoDTOMapper mapper;

    private final ClienteService clienteService;

    private final LimiteCreditoService limiteCreditoService;

    private final RiscoService riscoService;

    @Autowired
    public ClienteLimiteCreditoRiscoServiceImpl(
            ClienteLimiteCreditoRiscoDAO clienteLimiteCreditoRiscoDAO,
            ClienteLimiteCreditoRiscoDTOMapper mapper,
            ClienteService clienteService,
            LimiteCreditoService limiteCreditoService,
            RiscoService riscoService) {
        this.clienteLimiteCreditoRiscoDAO = clienteLimiteCreditoRiscoDAO;
        this.mapper = mapper;
        this.clienteService = clienteService;
        this.limiteCreditoService = limiteCreditoService;
        this.riscoService = riscoService;
    }

    @Transactional
    @Override
    public ClienteLimiteCreditoRiscoDTO salvar(ClienteLimiteCreditoRiscoDTO clienteLimiteCreditoRiscoDTO) {
        ClienteLimiteCreditoRisco clienteLimiteCreditoRisco = mapper.toModel(clienteLimiteCreditoRiscoDTO);

        Cliente cliente = clienteService.salvarOuCarregarPorNome(clienteLimiteCreditoRisco.getCliente());

        LimiteCredito limiteCredito = limiteCreditoService.salvarOuCarregarPorValorLimite(clienteLimiteCreditoRisco.getLimiteCredito());

        Risco risco = riscoService.salvarOuCarregarPorTipoRisco(clienteLimiteCreditoRisco.getRisco());

        ClienteLimiteCreditoRisco save = clienteLimiteCreditoRiscoDAO.save(new ClienteLimiteCreditoRisco(cliente, limiteCredito, risco));

        return mapper.toDTO(save);
    }

    @Override
    public List<ClienteLimiteCreditoRiscoDTO> listarTodos() {
        return clienteLimiteCreditoRiscoDAO.findAll()
                .stream()
                .map(mapper::toDTO)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void deletar(ClienteLimiteCreditoRiscoDTO clienteLimiteCreditoRiscoDTO) {
        ClienteLimiteCreditoRisco clienteLimiteCreditoRisco = mapper.toModel(clienteLimiteCreditoRiscoDTO);

        Cliente cliente = clienteService.salvarOuCarregarPorNome(clienteLimiteCreditoRisco.getCliente());

        LimiteCredito limiteCredito = limiteCreditoService.salvarOuCarregarPorValorLimite(clienteLimiteCreditoRisco.getLimiteCredito());

        Risco risco = riscoService.salvarOuCarregarPorTipoRisco(clienteLimiteCreditoRisco.getRisco());

        ClienteLimiteCreditoRisco aDeletar = new ClienteLimiteCreditoRisco(cliente, limiteCredito, risco);

        try {
            clienteLimiteCreditoRiscoDAO.delete(aDeletar);
        } catch (Exception e) {
            throw new NegocioException("Erro ao apagar registro", e);
        }
    }

    @Transactional
    @Override
    public void deletar(String nome, String limiteCredito, String tipoRisco) {
        deletar(new ClienteLimiteCreditoRiscoDTO(nome, limiteCredito, tipoRisco, null));
    }

}
