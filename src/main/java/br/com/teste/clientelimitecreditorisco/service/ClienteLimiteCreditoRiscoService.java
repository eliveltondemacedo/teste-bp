/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.teste.clientelimitecreditorisco.service;

import java.util.List;

import br.com.teste.clientelimitecreditorisco.mapping.ClienteLimiteCreditoRiscoDTO;

/**
 *
 * @author Elivelton de Macedo
 */
public interface ClienteLimiteCreditoRiscoService {

    ClienteLimiteCreditoRiscoDTO salvar(ClienteLimiteCreditoRiscoDTO clienteLimiteCreditoRiscoDTO);

    List<ClienteLimiteCreditoRiscoDTO> listarTodos();

    void deletar(ClienteLimiteCreditoRiscoDTO clienteLimiteCreditoRiscoDTO);

    void deletar(String nome, String limiteCredito, String tipoRisco);
    
}
