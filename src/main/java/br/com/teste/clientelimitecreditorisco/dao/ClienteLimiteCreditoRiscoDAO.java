/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.teste.clientelimitecreditorisco.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.teste.clientelimitecreditorisco.model.ClienteLimiteCreditoRisco;
import br.com.teste.clientelimitecreditorisco.model.ClienteLimiteCreditoRiscoPK;

/**
 *
 * @author Elivelton de Macedo
 */
public interface ClienteLimiteCreditoRiscoDAO extends JpaRepository<ClienteLimiteCreditoRisco, ClienteLimiteCreditoRiscoPK> {

}
