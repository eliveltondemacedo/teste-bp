/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.teste.cliente.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.teste.cliente.model.Cliente;
 
/**
 *
 * @author Elivelton de Macedo
 */
public interface ClienteDAO extends JpaRepository<Cliente, Long>{
    Cliente findByNome(String nome);
}
