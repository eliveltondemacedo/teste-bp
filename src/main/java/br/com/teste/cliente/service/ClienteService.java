/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.teste.cliente.service;

import br.com.teste.cliente.model.Cliente;

/**
 *
 * @author Elivelton de Macedo
 */
public interface ClienteService {

    Cliente salvarOuCarregarPorNome(Cliente cliente);

}
