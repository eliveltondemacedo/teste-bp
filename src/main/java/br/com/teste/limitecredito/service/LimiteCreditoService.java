/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.teste.limitecredito.service;

import br.com.teste.limitecredito.model.LimiteCredito;

/**
 *
 * @author Elivelton de Macedo
 */
public interface LimiteCreditoService {

    public LimiteCredito salvarOuCarregarPorValorLimite(LimiteCredito limiteCredito);

}
