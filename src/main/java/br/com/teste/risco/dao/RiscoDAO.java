package br.com.teste.risco.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.teste.risco.model.Risco;
import br.com.teste.risco.model.TipoRisco;

/**
 *
 * @author Elivelton de Macedo
 */
public interface RiscoDAO extends JpaRepository<Risco, Long> {

    public Risco findByTipoRisco(TipoRisco tipoRisco);

}
