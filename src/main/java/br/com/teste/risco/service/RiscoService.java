/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.teste.risco.service;

import br.com.teste.risco.model.Risco;

/**
 *
 * @author Elivelton de Macedo Bispo
 */
public interface RiscoService {

    public Risco salvarOuCarregarPorTipoRisco(Risco risco);

}
