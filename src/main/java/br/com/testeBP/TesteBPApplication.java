package br.com.testeBP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteBPApplication {

    public static void main(String[] args) {
        SpringApplication.run(TesteBPApplication.class, args);
    }
}
